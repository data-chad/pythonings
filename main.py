import logging
import sys
import time

import typer
import yaml
from progress.bar import Bar
from rich import print
from watchdog.events import LoggingEventHandler
from watchdog.observers import Observer

SKIP_CHECKS = []

def load_exercises():
    with open("exercises/exercises.yaml") as f:
        exercises = yaml.safe_load(f)
    return exercises


if __name__ == "__main__":
    print("Welcome to [bold magenta]Pythonings[/bold magenta]!", ":snake:")

    exercises = load_exercises().get('exercises', [])

    bar = Bar('Progress :',  fill='#', empty_fill= "-", color="green",  max=20, suffix=' (%(index)d/%(max)d) %(percent)d%%')
    for i in range(15):
        time.sleep(0.1)
        bar.next()
    bar.finish()

    event_handler = LoggingEventHandler()
    observer = Observer()
    observer.schedule(event_handler, './exercises/', recursive=True)
    observer.start()
    try:
        while True:
            time.sleep(1)
    finally:
        observer.stop()
        observer.join()
        exit(0)